import simpy
import numpy as np

class Queue_FIFO():
    def __init__(self, env, arrival_rate, service_rate, Nservers, Ncustomers, track_customers = False):
        self.track_customers = track_customers
        
        # model parameters
        # to do: make arrival_rate and service_rate function from system_load
        self.arrival_rate = arrival_rate
        self.service_rate = service_rate
        self.system_load = arrival_rate / (Nservers*service_rate)
        self.Ncustomers = Ncustomers
        
        # SimPy environment setup
        self.env = env
        self.server = simpy.Resource(self.env, capacity = Nservers)
        

        # for keeping track of process results
        self.waiting_time = np.zeros(Ncustomers)
        self.service_time = np.zeros(Ncustomers)
        self.current_no_customers = 0
        
        # tracking time and number of customers in the system when events happen 
        self.cust_dict = {0:0}
        
        # self.customer_counter = 0

        # start the running process everytime an instance is created
        # self.action = env.process(self.run())

    def run(self):
        self.env.process(self.source())
        self.env.run()

        return self.waiting_time, self.service_time, self.cust_dict

    def source(self):
        """Generating customers"""

        for i in range(self.Ncustomers):
            t_interarrival = np.random.exponential(1/self.arrival_rate)
            yield self.env.timeout(t_interarrival)
            c = self.customer(i)
            self.env.process(c)
        
    def customer(self, i):
        """Generates a customer, customer arrives, is serviced and leaves the system"""
        arrival_time = self.env.now
        #print('Customer ', i,  'arrives at ', arrival_time)
        
        # keep track of number of customers in the system at this time
        self.current_no_customers += 1
        
        if self.track_customers:
            # round time to 8 decimals (?) --> need to round so that 
            self.cust_dict[round(arrival_time,8)] = self.current_no_customers
        
        with self.server.request() as req:
            # wait for the server
            yield req

            # calculate and save waiting time 
            self.waiting_time[i] = self.env.now - arrival_time
            
            # customer gets serviced now
            #print('Customer', i, ' getting serviced at ', self.env.now)

            service_time = np.random.exponential(1/self.service_rate) # check if this is correct
            yield self.env.timeout(service_time)
            #print('Customer', i, 'finished now at', self.env.now)
            # save service_time
            self.service_time[i] = service_time
            
            # keep track of number of customers in the system
            self.current_no_customers -= 1
            self.cust_dict[round(self.env.now, 8)] = self.current_no_customers
            #self.customers_in_system.append(self.current_no_customers)


class Queue_Shortest_Job():
    def __init__(self, env, arrival_rate, service_rate, Nservers, Ncustomers):
        
        # model parameters
        self.arrival_rate = arrival_rate
        self.service_rate = service_rate
        #?self.system_load = arrival_rate / (Nservers*service_rate)
        self.Ncustomers = Ncustomers
        
        # SimPy environment setup
        self.env = env
        self.server = simpy.PriorityResource(self.env, capacity=Nservers)
        
        # for keeping track of process results
        self.waiting_time = np.zeros(Ncustomers)
        self.service_time = np.zeros(Ncustomers)
        
    def run(self):
        self.env.process(self.source())
        self.env.run()
        
        return self.waiting_time, self.service_time
    
    def source(self):
        """Generating customers"""
        
        for i in range(self.Ncustomers):
            
            # get random arrival time
            t_interarrival = np.random.exponential(1/self.arrival_rate)
            yield self.env.timeout(t_interarrival)
            c = self.customer(i)
            self.env.process(c)
            
    def customer(self, i):
        """Generates a customer, customer arrives, is serviced and leaves system"""
        arrival_time = self.env.now

        # get random service time
        service_time = np.random.exponential(1/self.service_rate)
        prio = service_time 
        # PriorityResource requests integer priority but using floats
        # seems to give the same result

        with self.server.request(priority=prio) as req:
            # wait for the server
            yield req

            # calculate and save waiting time
            self.waiting_time[i] = self.env.now - arrival_time

            yield self.env.timeout(service_time)

            # save service time
            self.service_time[i] = service_time


class Queue_FIFO_DeterministicService():
    def __init__(self, env, arrival_rate, service_rate, Nservers, Ncustomers):
        
        # model parameters
        self.arrival_rate = arrival_rate
        self.service_rate = service_rate
        # self.system_load?
        self.Ncustomers = Ncustomers
        
        # SimPy environment setup
        self.env = env
        self.server = simpy.Resource(self.env, capacity = Nservers)
        
        # for keeping track of process results
        self.waiting_time = np.zeros(Ncustomers)
    
    def run(self):
        self.env.process(self.source())
        self.env.run()
        
        return self.waiting_time
    
    def source(self):
        """Generating customers"""
        
        for i in range(self.Ncustomers):
            # get random arrival time
            t_interarrival = np.random.exponential(1/self.arrival_rate)
            yield self.env.timeout(t_interarrival)
            c = self.customer(i)
            self.env.process(c)
            
    def customer(self, i):
        """Generates a customer, customer arrives, is serviced and leaves the system"""
        arrival_time = self.env.now
        
        with self.server.request() as req:
            # wait for server
            yield req
            
            # calculate and save waiting time
            self.waiting_time[i] = self.env.now - arrival_time
            
            # get deterministic service time as 1/service_rate
            yield self.env.timeout(1/self.service_rate)
            
class Queue_FIFO_LongtailService():
    def __init__(self, env, arrival_rate, service_rate, Nservers, Ncustomers):
        
        # model parameters

        self.arrival_rate = arrival_rate
        self.service_rate = service_rate
        self.system_load = arrival_rate / (Nservers*service_rate)
        self.Ncustomers = Ncustomers
        
        # SimPy environment setup
        self.env = env
        self.server = simpy.Resource(self.env, capacity = Nservers)

        # for keeping track of process results
        self.waiting_time = np.zeros(Ncustomers)
        self.service_time = np.zeros(Ncustomers)

    def run(self):
        self.env.process(self.source())
        self.env.run()

        return self.waiting_time, self.service_time

    def source(self):
        """Generating customers"""

        for i in range(self.Ncustomers):
            t_interarrival = np.random.exponential(1/self.arrival_rate)
            yield self.env.timeout(t_interarrival)
            c = self.customer(i)
            self.env.process(c)
        
    def customer(self, i):
        """Generates a customer, customer arrives, is serviced and leaves the system"""
        arrival_time = self.env.now
        service_beta_common = (1/self.service_rate)* 0.5
        service_beta_rare = (1/self.service_rate)* 2.5
        #print('Customer ', i,  'arrives at ', arrival_time)
        
        with self.server.request() as req:
            # wait for the server
            yield req

            # calculate and save waiting time
            # NOTE: Constant Service Rate 
            self.waiting_time[i] = self.env.now - arrival_time
            
            pointer = np.random.uniform()
            # Hyper-exponentially distributed service rate
            if pointer <= 0.25:
                service_time = np.random.exponential(service_beta_rare) # Long-tailed with beta = fate-tailed rate 
            else:
                service_time = np.random.exponential(service_beta_common) # Normal with beta = normal-tailed rate

            yield self.env.timeout(service_time)