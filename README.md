# Running the code
## Requirements
The following non-standard packages were used in our code:
- matplotlib
- numpy
- tqdm
- simpy
- scipy
- seaborn

These can be installed by running the following code

`pip install matplotlib`  
`pip install numpy`    
`pip install tqdm`  
`pip install simpy`
`pip install scipy`   
`pip install seaborn`  

## Running the code
The implementations of the algorithm and the simulations have been done in python files.
The python file Queues.py contains the classes which define queueing systems M/M/n, M/M/1/Prio, M/D/n, M/H/n.

The python file avg_waiting_time.py contains the running of simulations, the plotting of the analysis with changing levels of system load and the historgram plotting of the simulation analysis with different number of customers at different levels of system load.

The python file rho_CI.py contains the analysis of the simulation sample size required to get confidence interval of within 1% of mean, and the visualization of our analysis. 

The python file statistical_testing.py contains the histogram plotting of the final simulations obtained when system load is constant, and the statistical testing (Shapiro-Wilk test, Welch t-test, Fisher test) for the simulation results of different queueing systems. 

Note that some data used for generating the figures is already saved in the Data folder.
Figures are saved in the Figures folder. The codes which generate new simulation data have been commented out, to allow quick running of the code for generating the plots. It is mentioned when this is the case. Uncommenting and running these cells will mean new data generation and overwriting of the existing data, which might take some time to run.
