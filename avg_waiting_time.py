# import packages 
import simpy
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import seaborn as sns

from Queues import *

# create Data and Figures directory
import os
if not os.path.exists('Data'):
    os.makedirs('Data')
if not os.path.exists('Figures'):
    os.makedirs('Figures')
if not os.path.exists('Data/waitingtimerho'):
    os.makedirs('Data/waitingtimerho')
if not os.path.exists('Data/customernumber'):
    os.makedirs('Data/customernumber')

##### Functions for generating simulations ########
def sim_waiting_time_per_rho(r_min=0.5, r_max=0.95, Nsim=50, nrho=15):

    """ Generate simulation for M/M/n queueing systems"""

    np.random.seed(59841)
    # running simulations for MM1, MM2, and MM4 queue
    ns = [1,2,4]
    Ncustomers = 50000
    # same rho for each queue
    rhos = np.linspace(r_min, r_max, nrho)
    arrival_rates = [1,2,4]

    # results = (queue, sim, waiting_time & service_time, customers)
    results = np.zeros((len(rhos),len(ns),Nsim,2,Ncustomers))
    for r, rho in tqdm(enumerate(rhos)):
        # same mu for each queue
        mu = 1 / rho
        
        for sim in range(Nsim):
            for i, n in enumerate(ns):
                # arrival rates scaled proportionally
                arrival_rate = arrival_rates[i]

                env = simpy.Environment()
                queue = Queue_FIFO(env, arrival_rate=arrival_rate, service_rate=mu, Nservers=n, Ncustomers=Ncustomers)
                results[r, i, sim, 0, :], results[r, i, sim, 1, :], _ = queue.run()
    
    # mean over the last 1000:-1000 waiting times for each rho, n, Nsim
    waiting_time_end_mean = np.mean(results[:,:,:,0,1000:-1000], axis=3)

    fileName = 'waitingtimeperrho_n124_samelambda_nrho' + str(nrho) + '_Nsim' + str(Nsim)
    np.save('Data/waitingtimerho/'+fileName, waiting_time_end_mean)
    
    return results

def sim_waiting_time_per_rho_SJF(r_min=0.5, r_max=0.95, Nsim=50, nrho=15):

    """ Generate simulation for M/M/1-Prio queueing systems"""

    np.random.seed(59841)
    # running simulations for MM1, MM2, and MM4 queue
    ns = [1]
    Ncustomers = 50000
    # same rho for each queue
    rhos = np.linspace(r_min, r_max, nrho)
    arrival_rates = [1]

    # results = (queue, sim, waiting_time & service_time, customers)
    results = np.zeros((len(rhos),len(ns),Nsim,2,Ncustomers))
    for r, rho in tqdm(enumerate(rhos)):
        # mu needs to be comparable to M/M/4 queue for plotting with n=1,2,4 queues
        mu = 1 / rho
        
        for sim in range(Nsim):
            for i, n in enumerate(ns):
                # arrival rates scaled proportionally
                arrival_rate = arrival_rates[i]

                env = simpy.Environment()
                queue = Queue_Shortest_Job(env, arrival_rate=arrival_rate, service_rate=mu, Nservers=n, Ncustomers=Ncustomers)
                results[r, i, sim, 0, :], results[r, i, sim, 1, :] = queue.run()
    
    # mean over the last 1000:-1000 waiting times for each rho, n, Nsim
    waiting_time_end_mean = np.mean(results[:,:,:,0,1000:-1000], axis=3)
    
    fileName = 'SJF_waitingtimeperrho_n1_lambda0.25_nrho' + str(nrho) + '_Nsim' + str(Nsim)
    np.save('Data/waitingtimerho/'+fileName, waiting_time_end_mean)
   
    return results

def sim_waiting_time_per_rho_DeterministicService(r_min=0.5, r_max=0.95, Nsim=50, nrho=15):

    """ Generate simulation for M/D/n queueing systems"""

    np.random.seed(59841)
    # running simulations for MM1, MM2, and MM4 queue
    ns = [1,2,4]
    Ncustomers = 50000
    # same rho for each queue
    rhos = np.linspace(r_min, r_max, nrho)
    arrival_rates = [1,2,4]

    # results = (queue, sim, waiting_time & service_time, customers)
    results = np.zeros((len(rhos),len(ns),Nsim,2,Ncustomers))
    for r, rho in tqdm(enumerate(rhos)):
        # same mu for each queue
        mu = 1 / rho
        
        for sim in range(Nsim):
            for i, n in enumerate(ns):
                # arrival rates scaled proportionally
                arrival_rate = arrival_rates[i]

                env = simpy.Environment()
                queue = Queue_FIFO_DeterministicService(env, arrival_rate=arrival_rate, service_rate=mu, Nservers=n, Ncustomers=Ncustomers)
                results[r, i, sim, 0, :] = queue.run()
    
    # mean over the last 1000:-1000 waiting times for each rho, n, Nsim
    waiting_time_end_mean = np.mean(results[:,:,:,0,1000:-1000], axis=3)

    fileName = 'DeterministicService_waitingtimeperrho_n124_samelambda_nrho' + str(nrho) + '_Nsim' + str(Nsim)
    np.save('Data/waitingtimerho/'+fileName, waiting_time_end_mean)
    
    return results

def sim_waiting_time_per_rho_Longtail_Service(r_min=0.5, r_max=0.95, Nsim=50, nrho=15):

    """ Generate simulation for M/H/n queueing systems"""

    np.random.seed(59841)

    # running simulations for MM1, MM2, and MM4 queue
    ns = [1,2,4]
    Ncustomers = 50000
    arrival_rates = [1,2,4]
    rhos = np.linspace(r_min, r_max, nrho)
    # All the rho will be predetermined by fixed lambda and mu

    # results = (queue, sim, waiting_time & service_time, customers)
    results = np.zeros((len(rhos),len(ns),Nsim,2,Ncustomers))
        
    for r, rho in tqdm(enumerate(rhos)):
        # same mu for each queue
        mu = 1 / rho
        
        for sim in range(Nsim):
            for i, n in enumerate(ns):
                # arrival rates scaled proportionally
                arrival_rate = arrival_rates[i]

                env = simpy.Environment()
                queue = Queue_FIFO_LongtailService(env, arrival_rate=arrival_rate, service_rate=mu, Nservers=n, Ncustomers=Ncustomers)
                results[r, i, sim, 0, :], results[r, i, sim, 1, :] = queue.run()
    # mean over the last 1000:-1000 waiting times for each rho, n, Nsim
    waiting_time_end_mean = np.mean(results[:,:,:,0,1000:-1000], axis=3)
    
    fileName = 'Longtail_waitingtimeperrho_waitingtimeperrho_n124_samelambda_nrho' + str(nrho) + '_Nsim' + str(Nsim)
    np.save('Data/waitingtimerho/'+fileName, waiting_time_end_mean)

    return results


def plot_n124_FIFO(r_min=0.5, r_max=0.95, Nsim=50, nrho=15, save = False, show=False):

    """ Plot the simulation of M/M/n systems with varying system loads"""
    
    # same rho for each queue
    rhos = np.linspace(r_min, r_max, nrho)

    arrival_rates = [1,2,4]

    fileName = 'waitingtimeperrho_n124_samelambda_nrho' + str(nrho) + '_Nsim' + str(Nsim)
    waiting_time_end_mean = np.load('Data/waitingtimerho/'+fileName+'.npy')

    # mean over the simulations
    waiting_time_meanmean = np.mean(waiting_time_end_mean, axis=2)
    waiting_time_meanstd = np.std(waiting_time_end_mean, axis=2)

    labels = ['M/M/1', 'M/M/2', 'M/M/4']
    colors = ['magenta', 'darkturquoise', 'limegreen']
    fig, ax = plt.subplots(1,1,figsize=(10,5))
    for i in [0,1,2]:
        ax.plot(rhos, waiting_time_meanmean[:,i], color=colors[i], label=rf'$\lambda=${arrival_rates[i]} ({labels[i]})')
        ax.fill_between(rhos, waiting_time_meanmean[:,i]-waiting_time_meanstd[:,i], waiting_time_meanmean[:,i]+waiting_time_meanstd[:,i], color=colors[i], alpha=0.2)

    ax.set_xlabel(r'System load $\rho$', fontsize=16)
    ax.set_ylabel('Time', fontsize=16)
    ax.set_title(rf'Mean waiting time per system load $\rho$, $\mu=1/\rho$; {Nsim} simulations', fontsize=18)
    ax.legend(fancybox=True, shadow=True, fontsize=14)
    ax.tick_params(axis='both', labelsize=14)
    ax.grid()
    fig.tight_layout()
    if save:
        fig.savefig('Figures/avgwaitingtime_rho_n124.svg')
    if show:
        plt.show()

def plot_n124_FIFO_n1_SJF(r_min=0.5, r_max=0.95, Nsim=50, nrho=15, save = False, show=False):
    
    """ Plot the simulation of M/M/n systems (including M/M/1/Prio) with varying system loads"""

    # plotting together
    # same rho for each queue
    rhos = np.linspace(r_min, r_max, nrho)

    arrival_rates = [1,2,4]

    fileName = 'waitingtimeperrho_n124_samelambda_nrho' + str(nrho) + '_Nsim' + str(Nsim)
    waiting_time_end_mean = np.load('Data/waitingtimerho/'+fileName+'.npy')

    fileName = 'SJF_waitingtimeperrho_n1_lambda0.25_nrho' + str(nrho) + '_Nsim' + str(Nsim)
    SJF_waiting_time_end_mean = np.load('Data/waitingtimerho/'+fileName+'.npy')

    # mean over the simulations
    waiting_time_meanmean = np.mean(waiting_time_end_mean, axis=2)
    waiting_time_meanstd = np.std(waiting_time_end_mean, ddof=1, axis=2)

    SJF_waiting_time_meanmean = np.mean(SJF_waiting_time_end_mean, axis=2)
    SJF_waiting_time_meanstd = np.std(SJF_waiting_time_end_mean, ddof=1, axis=2)


    labels = ['M/M/1', 'M/M/2', 'M/M/4']
    colors = ['magenta', 'darkturquoise', 'limegreen']
    fig, ax = plt.subplots(1,1,figsize=(10,5))
    for i in [0,1,2]:
        ax.plot(rhos, waiting_time_meanmean[:,i], color=colors[i], label=rf'{labels[i]} ($\lambda=${arrival_rates[i]})')
        ax.fill_between(rhos, waiting_time_meanmean[:,i]-waiting_time_meanstd[:,i], waiting_time_meanmean[:,i]+waiting_time_meanstd[:,i], color=colors[i], alpha=0.2)

    ax.plot(rhos, SJF_waiting_time_meanmean[:,0], color='k', label=rf'M/M/1/Prio ($\lambda={arrival_rates[0]}$)')
    ax.fill_between(rhos, SJF_waiting_time_meanmean[:,0]-SJF_waiting_time_meanstd[:,0], SJF_waiting_time_meanmean[:,0]+SJF_waiting_time_meanstd[:,0], color='k', alpha=0.2)

    ax.set_xlabel(r'System load $\rho$', fontsize=16)
    ax.set_ylabel('Time', fontsize=16)
    ax.set_title(rf'FIFO vs SJF - Mean waiting time per $\rho$, $\mu=1/\rho$; {Nsim} simulations', fontsize=18)
    ax.legend(fancybox=True, shadow=True, fontsize=14)
    ax.tick_params(axis='both', labelsize=14)

    ax.grid()
    fig.tight_layout()

    if save:
        fig.savefig('Figures/avgwaitingtimeFIFOandSJF.svg')

    if show:    
        plt.show()

def plot_n1_FIFO_n124D(r_min=0.5, r_max=0.95, Nsim=50, nrho=15, save = False, show=False):

    """ Plot the simulation of M/D/n systems with varying system loads"""
    
    # plotting together
    # same rho for each queue
    rhos = np.linspace(r_min, r_max, nrho)

    arrival_rates = [1,2,4]

    fileName = 'DeterministicService_waitingtimeperrho_n124_samelambda_nrho' + str(nrho) + '_Nsim' + str(Nsim)
    DET_waiting_time_end_mean = np.load('Data/waitingtimerho/'+fileName+'.npy')

    # mean over the simulations

    DET_waiting_time_meanmean = np.mean(DET_waiting_time_end_mean, axis=2)
    DET_waiting_time_meanstd = np.std(DET_waiting_time_end_mean, ddof=1, axis=2)

    labelsD = ['M/D/1', 'M/D/2', 'M/D/4']

    colorsD = ['mediumpurple', 'dodgerblue', 'forestgreen']
    fig, ax = plt.subplots(1,1,figsize=(10,5))
    
    for i in range(3):
        ax.plot(rhos, DET_waiting_time_meanmean[:,i], color=colorsD[i], label=rf'{labelsD[i]} ($\lambda=${arrival_rates[i]})')
        ax.fill_between(rhos, DET_waiting_time_meanmean[:,i]-DET_waiting_time_meanstd[:,i], DET_waiting_time_meanmean[:,i]+DET_waiting_time_meanstd[:,i], color=colorsD[i], alpha=0.2)

    ax.set_xlabel(r'System load $\rho$', fontsize=16)
    ax.set_ylabel('Time', fontsize=16)
    ax.set_title(rf'Deterministic - Mean waiting time per $\rho$, $\mu=1/\rho$; {Nsim} simulations', fontsize=18)
    ax.legend(fancybox=True, shadow=True, fontsize=14)
    ax.tick_params(axis='both', labelsize=14)

    ax.grid()
    fig.tight_layout()

    if save:
        fig.savefig('Figures/avgwaitingtimeDet.svg')

    if show:    
        plt.show()

def plot_longtail(r_min=0.5, r_max=0.95, Nsim=50, nrho=15, save = False, show=False):

    """ Plot the simulation of M/H/n systems with varying system loads"""
    
    # plotting together
    # same rho for each queue
    rhos = np.linspace(r_min, r_max, nrho)

    arrival_rates = [1,2,4]

    fileName = 'Longtail_waitingtimeperrho_waitingtimeperrho_n124_samelambda_nrho' + str(nrho) + '_Nsim' + str(Nsim)
    Long_waiting_time_end_mean = np.load('Data/waitingtimerho/'+fileName+'.npy')


    Long_waiting_time_meanmean = np.mean(Long_waiting_time_end_mean, axis=2)
    Long_waiting_time_meanstd = np.std(Long_waiting_time_end_mean, ddof=1, axis=2)

    labelsD = ['M/H/1', 'M/H/2', 'M/H/4']
    colorsD = ['blueviolet', 'deepskyblue', 'lawngreen']
    fig, ax = plt.subplots(1,1,figsize=(10,5))
    for i in range(3):
        ax.plot(rhos, Long_waiting_time_meanmean[:,i], color=colorsD[i], label=rf'{labelsD[i]} ($\lambda=${arrival_rates[i]})')
        ax.fill_between(rhos, Long_waiting_time_meanmean[:,i]-Long_waiting_time_meanstd[:,i], Long_waiting_time_meanmean[:,i]+Long_waiting_time_meanstd[:,i], color=colorsD[i], alpha=0.2)

    ax.set_xlabel(r'System load $\rho$', fontsize=16)
    ax.set_ylabel('Time', fontsize=16)
    ax.set_title(rf'Hyperexponential - Mean waiting time per $\rho$, $\mu=1/\rho$; {Nsim} simulations', fontsize=18)
    ax.legend(fancybox=True, shadow=True, fontsize=14, loc='upper left')
    ax.tick_params(axis='both', labelsize=14)

    ax.grid()
    fig.tight_layout()

    if save:
        fig.savefig('Figures/avgwaitingtimeFIFOandLong.svg')

    if show: 
        plt.show()

################## Analysis of simulations using different number of customers number via histograms ##################

def MMN_sim_waiting_time_per_rho(Ncustomers, r_min=0.5, r_max=0.95, Nsim=100, nrho=15):
    
    """ Generate simualtions of M/M/1 queueing system given number of customers and range of varying system loads"""

    np.random.seed(59841)
    # running simulations for MM1, MM2, and MM4 queue
    ns = [1]
    # same rho for each queue
    rhos = np.linspace(r_min, r_max, nrho)
    arrival_rates = [1]

    # results = (queue, sim, waiting_time & service_time, customers)
    results = np.zeros((len(rhos),len(ns),Nsim,2,Ncustomers))
    for r, rho in tqdm(enumerate(rhos)):
        # same mu for each queue
        mu = 1 / rho
        
        for sim in range(Nsim):
            for i, n in enumerate(ns):
                # arrival rates scaled proportionally
                arrival_rate = arrival_rates[i]

                env = simpy.Environment()
                queue = Queue_FIFO(env, arrival_rate=arrival_rate, service_rate=mu, Nservers=n, Ncustomers=Ncustomers)
                results[r, i, sim, 0, :], results[r, i, sim, 1, :], _ = queue.run()
    
    # mean over the last 1000:-1000 waiting times for each rho, n, Nsim
    start_pointer = int((1000/50000) * Ncustomers)
    end_pointer = - int((1000/50000) * Ncustomers)
    waiting_time_end_mean = np.mean(results[:,:,:,0,start_pointer:end_pointer], axis=3)

    fileName = 'MMN_n124_samelambda_nrho' + str(nrho) + '_Nsim' + str(Nsim) + '_Ncustomers' + str(Ncustomers)
    np.save('Data/customernumber/'+fileName, waiting_time_end_mean)

    return waiting_time_end_mean,rhos

def check_histogram_mm1():

    """ Visualize the simulations obtained from using different number of customers into histograms """

    Ncustomers_list = [100, 500, 5000, 50000]
    N_sim = 200
    results_list = []
    colors = ["#ff944d","#ff99ff","#00e6e6","#33ff33"]

    for n, Ncustomers in tqdm(enumerate(Ncustomers_list)):
        means,rhos = MMN_sim_waiting_time_per_rho(Ncustomers=Ncustomers,r_min=0.5, r_max=0.90, Nsim=N_sim, nrho=3)
        results_list.append(means)

    for r, rho in enumerate(rhos):
        plt.style.use('seaborn-darkgrid')
        sns.set_palette("hls") 
        # print a plot for each rho

        for n,Ncustomers in enumerate(Ncustomers_list):
            temp = results_list[n]
            temp = temp[r,0,:]
            sns.distplot(temp, norm_hist=True,kde_kws={"color":colors[n], "lw":2}, hist_kws={ "color": colors[n] },label=f'customers={Ncustomers}')

        plt.xlabel('Mean waiting time',fontsize=18)
        plt.ylabel('Density',fontsize=18)
        plt.title(f'The histogram of avg waiting time when rho={round(rho,2)}',fontsize=18)
        plt.yticks(fontsize=14)
        plt.xticks(fontsize=14)
        plt.legend(fontsize=14)
        plt.savefig(f'Figures/MM1_customer_numbers_histogram_{round(rho,2)}_sim_{N_sim}.svg')
        plt.savefig(f'Figures/MM1_customer_numbers_histogram_{round(rho,2)}_sim_{N_sim}.jpg')

        plt.show() 

            

if __name__ == '__main__':

    """ Execute the modules """

    #### Generate simulations ###

    ### uncomment below 4 lines of comments, if you wish to generate generate 50 simulations for the changing system load analysis ###
    # r_min = 0.05
    # r_max = 0.97
    # Nsim = 50
    # nrho = 15

    ### uncomment below 4 lines of comments, if you wish to generate generate 350 simulations for system load is constant value 0.8 ###
    r_min = 0.8
    r_max = 0.8
    Nsim = 350
    nrho = 1

    ###### UNCOMMENT the next code to generate new data, keep commented to use pregenerated data for plotting ######
    
    # ## Generate data for the FIFO queues n=1, n=2, n=4
    # results = sim_waiting_time_per_rho(r_min=r_min, r_max=r_max, Nsim=Nsim, nrho=nrho)
    
    # ## Generate data for shortest job priority (1 server)
    # results_SJF = sim_waiting_time_per_rho_SJF(r_min=r_min, r_max=r_max, Nsim=Nsim, nrho=nrho)
    
    # ## Generate data for deterministic service
    # results_Deterministic = sim_waiting_time_per_rho_DeterministicService(r_min=r_min, r_max=r_max, Nsim=Nsim, nrho=nrho)

    # ## Generate data for longtail service
    # results_Longtail = sim_waiting_time_per_rho_Longtail_Service(r_min=r_min, r_max=r_max, Nsim=Nsim, nrho=nrho)


    ###### Plotting the results for the simulations Note: only for the simulation results of changing system load #######
    r_min = 0.05
    r_max = 0.97
    Nsim = 50
    nrho = 15
    plot_n124_FIFO(r_min=r_min, r_max=r_max, Nsim=Nsim, nrho=nrho, save = False, show=True)
    plot_n124_FIFO_n1_SJF(r_min=r_min, r_max=r_max, Nsim=Nsim, nrho=nrho, save = False, show=True)
    plot_n1_FIFO_n124D(r_min=r_min, r_max=r_max, Nsim=Nsim, nrho=nrho, save = False, show=True)
    plot_longtail(r_min=r_min, r_max=r_max, Nsim=Nsim, nrho=nrho, save = False, show=True)

    ###### Uncomment the below to generate the simulations for the analysis of different number of customers at different level of system load rho #######

    #check_histogram_mm1()
