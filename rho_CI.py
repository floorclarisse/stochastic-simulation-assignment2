import simpy
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

# create Data and Figures directory
import os
if not os.path.exists('Data'):
    os.makedirs('Data')
if not os.path.exists('Figures'):
    os.makedirs('Figures')

from scipy.stats import norm

from Queues import *

def get_necessary_sample_size_for_CI(fileName, k_min = 10, k_max = 500, p=0.95, nrho=15):
    """Generates necessary sample size to get confidence interval of within 1% of mean
    
        Args:
            k_min (int): minimum number simulations to run
            k_max (int): maximum number of simulations to obtain necessary sample size
            p (float): required confidence level/2, must be between 0 and 1
            nrho (int): number of rhos between 0.5 and 0.999 to run for

        Returns:
            sample_necessary: numpy array of shape (len(rhos),3) containing necessary sample size
        """
    
    np.random.seed(59841)
    # running simulations for MM1, MM2, and MM4 queue
    ns = [1,2,4]
    Ncustomers = 25000
    # same rho for each queue
    rhos = np.linspace(0.5, 0.97, nrho)
    arrival_rates = [1,2,4]

    # critical_value
    CV = norm.ppf(p+(1-p)/2)
    
    # results = (queue, sim, k_max), saving average waiting time per iteration
    results = np.zeros((len(rhos),len(ns),k_max))
    # save necessary sample size
    sample_necessary = np.full((len(rhos),len(ns)),k_min)
    means_final = np.zeros((len(rhos), len(ns)))
    stdevs_final = np.zeros((len(rhos), len(ns)))

    for r, rho in tqdm(enumerate(rhos)):
        # same mu for each queue, but dependent on rho
        mu = 1 / rho
        
        for i, n in enumerate(ns):
            # arrival rates scaled proportionally
            arrival_rate = arrival_rates[i]
            
            k = k_min
            
            # get the first k_min samples
            for sim in range(k):
                # initial sample
                env = simpy.Environment()
                queue = Queue_FIFO(env, arrival_rate=arrival_rate, service_rate=mu, Nservers=n, Ncustomers=Ncustomers)
                waitingtimes = queue.run()[0]
                results[r, i, sim] = np.mean(waitingtimes[1000:-1000])
            
            # average over the k_min simulations
            waiting_time_sample_mean = np.mean(results[r,i,:k])
            waiting_time_sample_std = np.std(results[r,i,:k], ddof=1)

            size_CI = 2 * CV * waiting_time_sample_std / np.sqrt(k)           
            
            # continue generating samples until the confidence interval is of desired length
            pbar = tqdm(total=k_max-k_min)
            # run extra simulations until the confidence interval is small enough
            # want the CI to be mean+-1%ofmean --> so d keeps changing?
            d = waiting_time_sample_mean / 100
            while size_CI > 2*d and k <= k_max-1:
            
                env = simpy.Environment()
                queue = Queue_FIFO(env, arrival_rate=arrival_rate, service_rate=mu, Nservers=n, Ncustomers=Ncustomers)
                waitingtimes = queue.run()[0]
                results[r, i, k] = np.mean(waitingtimes[1000:-1000]) # filter out first and last 1000 waiting times
        
                # update sample mean and standard deviation, need to include the just done simulation at index k
                waiting_time_sample_mean = np.mean(results[r,i,:k+1])
                waiting_time_sample_std = np.std(results[r,i,:k+1], ddof=1)

                k += 1
                size_CI = 2 * CV * waiting_time_sample_std / np.sqrt(k)      
                
                d = waiting_time_sample_mean / 100

                pbar.update(1)
            pbar.close()
            
            # keep track of how many iterations needed
            sample_necessary[r,i] = k
            means_final[r,i] = waiting_time_sample_mean
            stdevs_final[r,i] = waiting_time_sample_std
    
    #fileName = 'necessarysamplesize_nrho' + str(nrho) + '_kminkmax' + str(k_min) + str(k_max) + '_p' + str(p)
    
    if not os.path.exists('Data/samplesize'):
        os.makedirs('Data/samplesize')
    
    np.save('Data/samplesize/meansfinal_'+fileName, means_final)
    np.save('Data/samplesize/stdevsfinal_' +fileName, stdevs_final)
    np.save('Data/samplesize/samplesize_' + fileName, sample_necessary)
    np.save('Data/samplesize/rho_s'+fileName, rhos)
    
    return sample_necessary, results, rhos

def plot_required_samplesize(fileName, save=False):
    p=0.95

    # critical_value
    CV = norm.ppf(p+(1-p)/2)

    # load the data    
    sample_nec = np.load('Data/samplesize/samplesize_'+fileName+'.npy')
    means_final = np.load('Data/samplesize/meansfinal_'+fileName+'.npy')
    stdevs_final = np.load('Data/samplesize/stdevsfinal_'+fileName+'.npy')
    rhos = np.load('Data/samplesize/rho_s'+fileName+'.npy')

    size_CI = stdevs_final * CV / np.sqrt(sample_nec)

    fig, axs = plt.subplots(1,3,figsize=(20,5))
    
    arrival_rates = [1,2,4]
    labels = ['M/M/1', 'M/M/2', 'M/M/4']
    colors = ['magenta', 'darkturquoise', 'limegreen']
    for n in range(3):
        axs[0].plot(rhos, sample_nec[:,n], color=colors[n], label=rf'$\lambda=${arrival_rates[n]} ({labels[n]})')
        axs[1].plot(rhos, means_final[:,n], color=colors[n], label=rf'$\lambda=${arrival_rates[n]} ({labels[n]})')
        axs[1].fill_between(rhos, means_final[:,n]-stdevs_final[:,n], means_final[:,n]+stdevs_final[:,n],color=colors[n], alpha=0.2)
        
        axs[2].plot(rhos, size_CI[:,n], color=colors[n], label=rf'$\lambda=${arrival_rates[n]} ({labels[n]})')



    axs[0].set_xlabel(r'System Load $\rho$',fontsize=14)
    axs[0].set_ylabel('Necessary Sample Size',fontsize=14)
    axs[0].set_title(f'Required sample for 95%-confidence interval', fontsize=14)
    axs[1].set_xlabel(r'System Load $\rho$', fontsize=14)
    axs[1].set_ylabel('Waiting Time',fontsize=14)
    axs[1].set_title('Mean waiting time $\pm$ standard deviation', fontsize=14)
    axs[2].set_xlabel(r'System Load $\rho$', fontsize=14)
    axs[2].set_ylabel('Size CI', fontsize=14)
    axs[2].set_title('Actual eventual size of 95%-confidence interval', fontsize=14)

    fig.suptitle(rf'95%-Confidence Intervals and required sample size to obtain an interval of size mean$\pm 1\%$; service rate $\mu=1/\rho$', fontsize=16)
    

    for i in range(3):
        axs[i].grid()
        axs[i].tick_params(which='both', labelsize=13)

    axs[1].legend(fancybox=True, shadow=True, fontsize=14, loc='upper center')
    
    fig.tight_layout()
    
    if save:
        fig.savefig('Figures/requiredsamplesize.svg')
    
    plt.show()


if __name__ == '__main__':
    #sample_nec, results, rhos = get_necessary_sample_size_for_CI(k_min = 10, k_max = 1000, p=0.95, nrho=15)
    
    nrho = 15
    k_min=10
    k_max=1000
    p=0.95

    # critical_value
    CV = norm.ppf(p+(1-p)/2)

    fileName = 'necessarysamplesize_nrho' + str(nrho) + '_kminkmax' + str(k_min) + str(k_max) + '_p' + str(p)
    ### Uncomment the next line to run the simulations. This will take a very long time.
    ### keeping it commented will provide a plot with pregenerated data
    # sample_nec, results, rhos = get_necessary_sample_size_for_CI(fileName, k_min = k_min, k_max = k_max, p=p, nrho=nrho)

    plot_required_samplesize(fileName, save=True)