import numpy as np
import scipy
from scipy import stats
import seaborn as sns
import matplotlib.pyplot as plt

#### Implement Statistical Testing Modules ####

# Shapiro-Wilk test to examine normality of results from each sampling

def test_normality(data,p=0.95):
    """
    Perform the Shapiro-Wilk test for normality.
    The Shapiro-Wilk test tests the null hypothesis that the data was drawn from a normal distribution
    
    Argument: 
        data : tested sample population
        p : confidence level
    Returns:
        stats : test statistic of Shapiro-Wilk 
        pval : p-value of the test
    
    Note:
        If the p-value is less than significant level (1-p), we reject the null hypothesis that 
        there's no difference between the means and conclude that a significant 
        difference does exist. If the p-value is larger than 0.05, we cannot 
        conclude that a significant difference exists.
    """

    statics, pval = stats.shapiro(data)[0], stats.shapiro(data)[1]
    
    print(f'the w test statistic is {statics}, the p-value is {pval}')
    if pval <= (1-p):
        print(f'We reject H0, the sample is not normally distributed at {int(100*p)} confidence interval')
    else:
        print(f'We accept H0, the sample is tested to be normally distributed at {int(100*p)} confidence interval')
    return statics, pval

# Two-sample Welch test : compared expected value difference by different samplings

def welch_test(x1,x2,p):
    """
    Welch test to evaluate whether the expected value two groups samples equal
    H0: E(x1) = E(x2)
    
    Arguments
        x1 : samples group 1
        x2 : samples group 2
        p : the provided confidence interval
        
    Returns:
        act: the actual value of Welch test
        pval: p-value of the test
        res: the acceptance result of this test, True means do not reject H0, False means reject H0
    """
    
    x1_var = np.var(x1,ddof=1)
    x2_var = np.var(x2,ddof=1)
    x1_avg = np.mean(x1)
    x2_avg = np.mean(x2)
    
    pool_var = (x1_var / x1.size)  + (x2_var / x2.size) 
    act = (x1_avg-x2_avg) / (np.sqrt(pool_var))

    dof = (x1_var/x1.size + x2.var()/x2.size)**2 / ((x1_var/x1.size)**2 / (x1.size-1) + (x2.var()/x2.size)**2 / (x2.size-1))
    print(f'the Welch-T test statistic is {act}, the degree-of-freedom is {dof}')
    crit_val = scipy.stats.t.ppf(q=(p+1)/2,df=dof)
    res = False
    if abs(act) > crit_val:
        res = True
        print(f'We reject H0 with actual t-statistic = {act} at confidence level p = {p} (critical value = {crit_val})')
    else:
        print(f'We do not reject H0 with actual t-statistic = {act} at confidence level p = {p} (critical value = {crit_val})')

    return act, res, dof

# Two-sample F-Test / Fischer Test: compared the variance of different population

def f_test(x1,x2,p):
    act = max([x1.var()/x2.var(),x2.var()/x1.var()])
    crit_val1 = scipy.stats.f.ppf(1-(1-p)/2, x1.size-1, x2.size-1)
    crit_val2 = scipy.stats.f.ppf((1-p)/2, x1.size-1, x2.size-1)

    res = True
    if act > crit_val1 or act < crit_val2:
        res = False
        print(f'F test: We reject H0 with actual t-statistic = {act} at confidence level p = {p} (critical value = {crit_val2}-{crit_val1})')
    else:
        print(f'F test: We do not reject H0 with actual t-statistic = {act} at confidence level p = {p} (critical value = {crit_val2}-{crit_val1})')
    return act, res

########## Testing Functions for the Queuing Simulation Data ##########

def test_everygroup_normality(groups,p=0.95):

    """Check normality of the currecnt data"""

    for data in groups:
        test_normality(data,p)

def check_everygroup_basic_stats(groups):

    """basic statistical information: mean, variance, max"""

    text_output = ['M/M/1','M/M/2', 'M/M/4', 'M/M/1-Prio', 'M/D/1', 'M/D/2', 'M/D/4', 'M/H/1', 'M/H/2', 'M/H/4']
    for i, data in enumerate(groups):
        max_val = np.max(data)
        var_val = np.var(data,ddof=1)
        mean_val = np.mean(data)
        print(f'{text_output[i]} : mean = {mean_val}; variance = {var_val}; max = {max_val}')
        print('\n')

def three_sys_plot_hist(text_output, data_n1,data_n2,data_n4, name='test'):

    """Plot the historgram of data that needs to be compared"""

    colors = ["#ff944d","#ff99ff","#00e6e6"]
    plt.figure(figsize=(14,7))


    sns.distplot(data_n1,norm_hist=True,kde_kws={"color":colors[0], "lw":2}, hist_kws={ "color": colors[0] },label=text_output[0])
    sns.distplot(data_n2,norm_hist=True,kde_kws={"color":colors[1], "lw":2}, hist_kws={ "color": colors[1] },label=text_output[1])
    sns.distplot(data_n4,norm_hist=True,kde_kws={"color":colors[2], "lw":2}, hist_kws={ "color": colors[2] },label=text_output[2])


    
    plt.xlabel('Mean waiting time',fontsize=18)
    plt.ylabel('Density',fontsize=18)
    plt.title(f'The fitted histogram of the average waiting time using {len(data_n1)} simulations at rho= 0.8',fontsize=18)
    plt.yticks(fontsize=18)
    plt.xticks(fontsize=18)
    plt.legend(fontsize=18)
    plt.savefig('Figures/' + name + 'threesysplothist.svg')
    plt.show() 

def four_sys_plot_hist(text_output, data_n1,data_n2,data_n4,data_others, name='test'):

    """Plot the historgram of data that needs to be compared"""

    colors = ["#ff944d","#ff99ff","#00e6e6","#33ff33"]
    plt.figure(figsize=(14,7))
    sns.distplot(data_n1,norm_hist=True,kde_kws={"color":colors[0], "lw":2}, hist_kws={ "color": colors[0] },label=text_output[0])
    sns.distplot(data_n2,norm_hist=True,kde_kws={"color":colors[1], "lw":2}, hist_kws={ "color": colors[1] },label=text_output[1])
    sns.distplot(data_n4,norm_hist=True,kde_kws={"color":colors[2], "lw":2}, hist_kws={ "color": colors[2] },label=text_output[2])
    sns.distplot(data_others,norm_hist=True,kde_kws={"color":colors[3], "lw":2}, hist_kws={ "color": colors[3] },label=text_output[3])

    plt.xlabel('Mean waiting time',fontsize=18)
    plt.ylabel('Density',fontsize=18)
    plt.title(f'The fitted histogram of the average waiting time using {len(data_n1)} simulations at rho= 0.8',fontsize=18)
    plt.yticks(fontsize=18)
    plt.xticks(fontsize=18)
    plt.legend(fontsize=18)
    plt.savefig('Figures/' + name + 'foursysplothist.svg')
    plt.show() 


def check_t_test_same_sys(text_output, data_n1,data_n2,data_n4,p=0.95):

    """
    generate one-sided t-test to compare the data of different number of servers
    Input:
        text_output : the notations for the currect system, e.g. ['M/M/1','M/M/2', 'M/M/4']
        data_n1 : system with 1 server
        data_n2 : system with 2 server
        data_n3 : system with 4 server
    Operation:
        print statement of the T-test 
    """
    
    print(f'*** Compare {text_output[0]} and {text_output[1]}: ***')
    welch_test(data_n1,data_n2,p)
    print('\n')

    print(f'*** Compare {text_output[1]} and {text_output[2]}: ***')
    welch_test(data_n2,data_n4,p)
    print('\n')

    print(f'*** Compare {text_output[0]} and {text_output[2]}: ***')
    welch_test(data_n1,data_n4,p)

def check_f_test_same_sys(text_output, data_n1,data_n2,data_n4,p=0.95):

    """
    generate one-sided t-test to compare the data of different number of servers
    Input:
        text_output : the notations for the currect system, e.g. ['M/M/1','M/M/2', 'M/M/4']
        data_n1 : system with 1 server
        data_n2 : system with 2 server
        data_n3 : system with 4 server
    Operation:
        print statement of the T-test 
    """
    
    print(f'*** Compare {text_output[0]} and {text_output[1]}: ***')
    f_test(data_n1,data_n2,p)
    print('\n')

    print(f'*** Compare {text_output[1]} and {text_output[2]}: ***')
    f_test(data_n2,data_n4,p)
    print('\n')

    print(f'*** Compare {text_output[0]} and {text_output[2]}: ***')
    f_test(data_n1,data_n4,p)

def compare_prio_to_MMN(data_prio, data_n1,data_n2,data_n4,p=0.95):

    """Compare the average waiting time of M/M/1-Prio to different FIFO M/M/N system"""

    text_output = ['M/M/1-Prio', 'M/M/1','M/M/2', 'M/M/4']
    
    print(f'*** Compare {text_output[0]} and {text_output[1]}: ***')
    welch_test(data_prio,data_n1,p)
    print('\n')

    print(f'*** Compare {text_output[0]} and {text_output[2]}: ***')
    welch_test(data_prio,data_n2,p)
    print('\n')

    print(f'*** Compare {text_output[0]} and {text_output[3]}: ***')
    welch_test(data_prio,data_n4,p)

def compare_prio_to_MMN_ftest(data_prio, data_n1,data_n2,data_n4,p=0.95):

    """Compare the average waiting time of M/M/1-Prio to different FIFO M/M/N system"""

    text_output = ['M/M/1-Prio', 'M/M/1','M/M/2', 'M/M/4']
    
    print(f'*** Compare {text_output[0]} and {text_output[1]}: ***')
    f_test(data_prio,data_n1,p)
    print('\n')

    print(f'*** Compare {text_output[0]} and {text_output[2]}: ***')
    f_test(data_prio,data_n2,p)
    print('\n')

    print(f'*** Compare {text_output[0]} and {text_output[3]}: ***')
    f_test(data_prio,data_n4,p)


# pick the data for constant rho = the second biggest rho from what we use

def run_statistical_testing(Nsim=350, nrho=1):
    """
    Run all the statistical testing modules
    """

    fileName = 'waitingtimeperrho_n124_samelambda_nrho' + str(nrho) + '_Nsim' + str(Nsim)
    waiting_time_end_mean = np.load('Data/waitingtimerho/'+fileName+'.npy')

    fileName = 'SJF_waitingtimeperrho_n1_lambda0.25_nrho' + str(nrho) + '_Nsim' + str(Nsim)
    SJF_waiting_time_end_mean = np.load('Data/waitingtimerho/'+fileName+'.npy')

    fileName = 'DeterministicService_waitingtimeperrho_n124_samelambda_nrho' + str(nrho) + '_Nsim' + str(Nsim)
    DET_waiting_time_end_mean = np.load('Data/waitingtimerho/'+fileName+'.npy')

    fileName = 'Longtail_waitingtimeperrho_waitingtimeperrho_n124_samelambda_nrho' + str(nrho) + '_Nsim' + str(Nsim)
    Long_waiting_time_end_mean = np.load('Data/waitingtimerho/'+fileName+'.npy')

    # choose the data simulated at the second largest rho 
    # data ordering: [['M/M/1','M/M/2', 'M/M/4'], ['M/M/1-Prio'], ['M/D/1', 'M/D/2', 'M/D/4'], ['M/H/1', 'M/H/2', 'M/H/4']]
    data_group = [waiting_time_end_mean[0,0,:],waiting_time_end_mean[0,1,:],waiting_time_end_mean[0,2,:],SJF_waiting_time_end_mean[0,0,:],
                    DET_waiting_time_end_mean[0,0,:],DET_waiting_time_end_mean[0,1,:],DET_waiting_time_end_mean[0,2,:],Long_waiting_time_end_mean[0,0,:],
                    Long_waiting_time_end_mean[0,1,:],Long_waiting_time_end_mean[0,2,:]]
    
    check_everygroup_basic_stats(data_group)
    print('\n')

    test_everygroup_normality(data_group)
    print('\n')

    # M/M/N
    label_mmn = ['M/M/1','M/M/2', 'M/M/4']
    check_t_test_same_sys(label_mmn, data_group[0], data_group[1], data_group[2],p=0.95)
    print('\n')

    check_f_test_same_sys(label_mmn, data_group[0], data_group[1], data_group[2],p=0.95)
    three_sys_plot_hist(label_mmn, data_group[0], data_group[1], data_group[2], name='1')
    print('\n')
    
    # M/D/N
    label_mdn = ['M/D/1','M/D/2', 'M/D/4']
    check_t_test_same_sys(label_mdn, data_group[4], data_group[5], data_group[6],p=0.95)
    print('\n')

    check_f_test_same_sys(label_mdn, data_group[4], data_group[5], data_group[6],p=0.95)
    three_sys_plot_hist(label_mdn, data_group[4], data_group[5], data_group[6], name='2')
    print('\n')
    
    # M/H/N
    label_mhn = ['M/H/1','M/H/2', 'M/H/4']
    check_t_test_same_sys(label_mhn, data_group[7], data_group[8], data_group[9],p=0.95)
    print('\n')

    check_f_test_same_sys(label_mhn, data_group[7], data_group[8], data_group[9],p=0.95)
    three_sys_plot_hist(label_mhn, data_group[7], data_group[8], data_group[9], name='3')
    print('\n')
    
    label_4sys = ['M/M/1','M/M/2', 'M/M/4','M/M/1-Prio']
    compare_prio_to_MMN(data_group[3], data_group[0],data_group[1],data_group[2],p=0.95)
    print('\n')

    compare_prio_to_MMN_ftest(data_group[3], data_group[0],data_group[1],data_group[2],p=0.95)
    four_sys_plot_hist(label_4sys, data_group[0], data_group[1], data_group[2],data_group[3], name='4')

run_statistical_testing(Nsim=350, nrho=1)

#### Testing Results ####
# M/M/1 : mean = 3.2162859790350615; variance = 0.027689316382798923; max = 3.8984071919601


# M/M/2 : mean = 1.4206095291414669; variance = 0.005855567301488361; max = 1.6834193213910111     


# M/M/4 : mean = 0.5972354853875247; variance = 0.0016744090844296948; max = 0.7207122858032307    


# M/M/1-Prio : mean = 1.505240148230197; variance = 0.0020605963736907147; max = 1.6414558449809091


# M/D/1 : mean = 1.6034942408375001; variance = 0.0029974045347873333; max = 1.7835815261023982


# M/D/2 : mean = 0.7227613874884806; variance = 0.0008105844455177075; max = 0.8067883254489489


# M/D/4 : mean = 0.3082394309728289; variance = 0.0001770471639264422; max = 0.3507986518683283


# M/H/1 : mean = 5.6226442901954305; variance = 0.17067748799340202; max = 7.2386651678399385


# M/H/2 : mean = 2.433328965041147; variance = 0.03789465431087406; max = 3.2130127993090745


# M/H/4 : mean = 0.9965799628792346; variance = 0.007006417186167556; max = 1.332166558013169



# ##### Normality ######

# the w test statistic is 0.9780462980270386, the p-value is 3.563976497389376e-05
# We reject H0, the sample is not normally distributed at 95 confidence interval
# the w test statistic is 0.9911684989929199, the p-value is 0.03447827324271202
# We reject H0, the sample is not normally distributed at 95 confidence interval
# the w test statistic is 0.9938790798187256, the p-value is 0.17059879004955292
# We accept H0, the sample is tested to be normally distributed at 95 confidence interval
# the w test statistic is 0.9945582151412964, the p-value is 0.2502509355545044
# We accept H0, the sample is tested to be normally distributed at 95 confidence interval
# the w test statistic is 0.9929023385047913, the p-value is 0.09647943824529648
# We accept H0, the sample is tested to be normally distributed at 95 confidence interval
# the w test statistic is 0.993694543838501, the p-value is 0.1533849537372589
# We accept H0, the sample is tested to be normally distributed at 95 confidence interval
# the w test statistic is 0.9930945038795471, the p-value is 0.10804755240678787
# We accept H0, the sample is tested to be normally distributed at 95 confidence interval
# the w test statistic is 0.9821906089782715, the p-value is 0.00025327946059405804
# We reject H0, the sample is not normally distributed at 95 confidence interval
# the w test statistic is 0.9890779852867126, the p-value is 0.010141263715922832
# We reject H0, the sample is not normally distributed at 95 confidence interval
# the w test statistic is 0.9919816851615906, the p-value is 0.05588578060269356
# We accept H0, the sample is tested to be normally distributed at 95 confidence interval


# *** Compare M/M/1 and M/M/2: ***
# the Welch-T test statistic is 183.4209607714732, the degree-of-freedom is 489.9208432749743
# We reject H0 with actual t-statistic = 183.4209607714732 at confidence level p = 0.95 (critical value = 1.964817917694301)


# *** Compare M/M/2 and M/M/4: ***
# the Welch-T test statistic is 177.5147262803604, the degree-of-freedom is 533.059606596276
# We reject H0 with actual t-statistic = 177.5147262803604 at confidence level p = 0.95 (critical value = 1.9644242270879018)


# *** Compare M/M/1 and M/M/4: ***
# the Welch-T test statistic is 285.938290301531, the degree-of-freedom is 390.935898270795
# We reject H0 with actual t-statistic = 285.938290301531 at confidence level p = 0.95 (critical value = 1.9660506801875866)


# *** Compare M/D/1 and M/D/2: ***
# the Welch-T test statistic is 267.01175268954705, the degree-of-freedom is 524.4613999210949
# We reject H0 with actual t-statistic = 267.01175268954705 at confidence level p = 0.95 (critical value = 1.9644975159736648)


# *** Compare M/D/2 and M/D/4: ***
# the Welch-T test statistic is 246.76528948542122, the degree-of-freedom is 494.1364697918554
# We reject H0 with actual t-statistic = 246.76528948542122 at confidence level p = 0.95 (critical value = 1.9647764075007155)


# *** Compare M/D/1 and M/D/4: ***
# the Welch-T test statistic is 430.085572561926, the degree-of-freedom is 389.96872676247153
# We reject H0 with actual t-statistic = 430.085572561926 at confidence level p = 0.95 (critical value = 1.966065822088963)


# *** Compare M/H/1 and M/H/2: ***
# the Welch-T test statistic is 130.6481700099181, the degree-of-freedom is 496.3103699301615
# We reject H0 with actual t-statistic = 130.6481700099181 at confidence level p = 0.95 (critical value = 1.9647552779043498)


# *** Compare M/H/2 and M/H/4: ***
# the Welch-T test statistic is 126.84886156597803, the degree-of-freedom is 473.45562221820654
# We reject H0 with actual t-statistic = 126.84886156597803 at confidence level p = 0.95 (critical value = 1.964987146206347)


# *** Compare M/H/1 and M/H/4: ***
# the Welch-T test statistic is 205.31545617937806, the degree-of-freedom is 377.5236755679925
# We reject H0 with actual t-statistic = 205.31545617937806 at confidence level p = 0.95 (critical value = 1.966267603876362)


# *** Compare M/M/1-Prio and M/M/1: ***
# the Welch-T test statistic is -185.5892498381391, the degree-of-freedom is 400.804379572605
# We reject H0 with actual t-statistic = -185.5892498381391 at confidence level p = 0.95 (critical value = 1.965900369897909)


# *** Compare M/M/1-Prio and M/M/2: ***
# the Welch-T test statistic is 17.795252613736974, the degree-of-freedom is 568.0505747870698
# We reject H0 with actual t-statistic = 17.795252613736974 at confidence level p = 0.95 (critical value = 1.9641489075212624)


# *** Compare M/M/1-Prio and M/M/4: ***
# the Welch-T test statistic is 277.95627347585463, the degree-of-freedom is 690.4154284564752
# We reject H0 with actual t-statistic = 277.95627347585463 at confidence level p = 0.95 (critical value = 1.9634059190846773)

#########                F tests               ########
# *** Compare M/M/1 and M/M/2: ***
# F test: We reject H0 with actual t-statistic = 4.728716272420075 at confidence level p = 0.95 (critical value = 0.8104447652417274-1.2338903807981718)


# *** Compare M/M/2 and M/M/4: ***
# F test: We reject H0 with actual t-statistic = 3.497094799555972 at confidence level p = 0.95 (critical value = 0.8104447652417274-1.2338903807981718)


# *** Compare M/M/1 and M/M/4: ***
# F test: We reject H0 with actual t-statistic = 16.536769084855944 at confidence level p = 0.95 (critical value = 0.8104447652417274-1.2338903807981718)

# *** Compare M/D/1 and M/D/2: ***
# F test: We reject H0 with actual t-statistic = 3.697831301059494 at confidence level p = 0.95 (critical value = 0.8104447652417274-1.2338903807981718)


# *** Compare M/D/2 and M/D/4: ***
# F test: We reject H0 with actual t-statistic = 4.578353177430626 at confidence level p = 0.95 (critical value = 0.8104447652417274-1.2338903807981718)


# *** Compare M/D/1 and M/D/4: ***
# F test: We reject H0 with actual t-statistic = 16.929977686808158 at confidence level p = 0.95 (critical value = 0.8104447652417274-1.2338903807981718)

# *** Compare M/H/1 and M/H/2: ***
# F test: We reject H0 with actual t-statistic = 4.5039990757858765 at confidence level p = 0.95 (critical value = 0.8104447652417274-1.2338903807981718)


# *** Compare M/H/2 and M/H/4: ***
# F test: We reject H0 with actual t-statistic = 5.408563792873726 at confidence level p = 0.95 (critical value = 0.8104447652417274-1.2338903807981718)


# *** Compare M/H/1 and M/H/4: ***
# F test: We reject H0 with actual t-statistic = 24.360166324432214 at confidence level p = 0.95 (critical value = 0.8104447652417274-1.2338903807981718)


# *** Compare M/M/1-Prio and M/M/1: ***
# F test: We reject H0 with actual t-statistic = 13.43752553208897 at confidence level p = 0.95 (critical value = 0.8104447652417274-1.2338903807981718)


# *** Compare M/M/1-Prio and M/M/2: ***
# F test: We reject H0 with actual t-statistic = 2.8416857256719856 at confidence level p = 0.95 (critical value = 0.8104447652417274-1.2338903807981718)


# *** Compare M/M/1-Prio and M/M/4: ***
# F test: We do not reject H0 with actual t-statistic = 1.23064094243884 at confidence level p = 0.95 (critical value = 0.8104447652417274-1.2338903807981718)